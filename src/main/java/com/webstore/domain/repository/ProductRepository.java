package com.webstore.domain.repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.webstore.domain.Product;

public interface ProductRepository {
	List<Product> getAllProducts();
	Product getProductbyId(String productId);
	
	List<Product> getProductsByCategory(String category);
	
	Set<Product> getProductsByFilter(Map<String, List<String>> filterParams);
	
	void addProduct(Product product);
}
